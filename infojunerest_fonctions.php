<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/filtres');
include_spip('urls/arbo');

function get_traductions($objet) {
	$traductions = array();
	// Détermination du type de l'objet
	$type = "";
	foreach ($objet as $key => $value) {
		$matches = array();
		
		if (preg_match("/^id_(\w+)$/", $key, $matches)) {
			switch ($matches[1]) {
				case "article":
				case "rubrique":
					$type = $matches[1];
					break;
			}
		}
		if ($type != "") break;
	}

	// Récupération des traductions de l'objet
	$select = '*';
	$from = 'spip_' . $type . "s";
	$where = array(
		$matches[0] . "<>" . $objet[$matches[0]],
		"id_trad = " . $objet["id_trad"],
		"statut = 'publie'"
	);

	$res_articles = sql_allfetsel($select, $from, $where);
	foreach ($res_articles as $obj) {
		$traductions[] = array(
			"id" => $obj[$matches[0]],
			"language" => $obj["lang"]
		);
	}

	return $traductions;
}

function check_langue($valeur, &$erreur) {
	$list_langues = langues_collectionner('','','');
	$est_valide = false;

	foreach ($list_langues as $langue) {
		if ($valeur == $langue["title"]) {
			$est_valide = true;
			break;
		}
	}

	if (!$est_valide) {
		$erreur['type'] = 'critere_lang_inexistant';
	}

	return $est_valide;
}

function url_site() {
	return (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://" . $_SERVER['HTTP_HOST'];
}

function get_logo($objet, $primary) {
  $chercher_logo = charger_fonction('chercher_logo', 'inc');
  $logo = $chercher_logo($objet[$primary], $primary, "on");
  

  if ($logo[0]) {
    $logo = url_site() . "/" . $logo[0];
  } else {
    $res_parent = sql_allfetsel("*", "spip_rubriques", "id_rubrique=" . $objet["id_parent"]);
    
    switch ($primary) {
        case "id_article":
            $logo = get_logo($res_parent[0], "id_rubrique", "on");
            break;
        case "id_rubrique":
             $logo = get_logo($res_parent[0], "id_rubrique", "on");
            break;
        case "id_mot":
            $logo = url_site() . "/IMG/logo/rubon0.png";
            break;
    }
  }

  return $logo;
}

function get_rubrique($id, $filtres = []) {
	$rubrique = array();
	$select = "id_rubrique, titre, descriptif, maj, spip_rubriques.id_parent, profondeur, url, id_trad";
	$from = "spip_rubriques, spip_urls";
	$where = array(
		"id_rubrique=" . intval($id),
		"id_rubrique=id_objet",
		"type='rubrique'",
		"statut='publie'"
	);

	if ($filtres['langue']) {
		$where[] = "lang=" . sql_quote($filtres['langue']);
	}

	$res_rubrique = sql_allfetsel($select, $from, $where);

	if ($res_rubrique) {
		$rub = $res_rubrique[0];

		$rubrique = array(
			"id" => $rub["id_rubrique"],
			"type" => "rubriques",
			"title" => supprimer_numero($rub['titre']),
      		"descriptif" => $rub['descriptif'],
			"url" => url_site() . "/" .urls_arbo_generer_url_objet_dist($rub['id_rubrique'], "rubrique"),
      		"logo" => get_logo($rub, "id_rubrique"),
			"update" => $rub['maj'],
			"translations" => get_traductions($rub),
			"id_parent" => $rub['id_parent'],
			"profondeur" => $rub['profondeur']
		);


	}

	return $rubrique;
}

// Retourne les rubriques enfants d'une rubrique sous formes hiérarchique
function liste_enfants($id_rubrique, $isRecursive = true) {
	$retour = [];
	$select = "id_rubrique, titre, descriptif, maj, spip_rubriques.id_parent, url, id_trad";
	$from = "spip_rubriques, spip_urls";
	$where = array(
		"id_rubrique=id_objet",
		"type='rubrique'",
		"statut='publie'",
		"spip_rubriques.id_parent=" . intval($id_rubrique)
	);
	$orderby = array("titre");
	$result = sql_allfetsel($select, $from, $where, "", $orderby);

	if (count($result) == 0) {
		return false;
	}

	foreach ($result as $rubrique) {
		$rub = array(
			"id" => $rubrique['id_rubrique'],
			"type" => 'rubriques',
			"title" => supprimer_numero($rubrique['titre']),
      		"descriptif" => $rubrique['descriptif'],
			"url" => url_site() . "/" .urls_arbo_generer_url_objet_dist($rubrique['id_rubrique'], "rubrique"),
      		"logo" => get_logo($rubrique, 'id_rubrique'),
			"update" => $rubrique['maj'],
			"translations" => get_traductions($rubrique)
		);

		if ($enfants = liste_enfants($rubrique['id_rubrique']) && $isRecursive) {
			$rub['children'] = $enfants;
		}

		$retour[] = $rub;
  }

	return $retour;
}