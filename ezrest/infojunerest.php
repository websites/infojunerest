<?php
// Voir le fichier ezrest/prefixe.php.template du plugin ezrest pour le modèle

// TODO mettre en place de la pagination

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

//////////////////////////////////// LANGUES ///////////////////////////////

/**
 * Récupère les données de la collection langues
 *
 * @param array $conditions    Tableau des conditions SQL à appliquer au select et correspondant aux filtres
 *                             passés dans la requête.
 * @param array $filtres       Tableau des critères de filtrage additionnels à appliquer au select.
 * @param array $configuration Configuration de la collection.
 *
 * @return array Tableau des données de la collection.
 */
function langues_collectionner($conditions, $filtres, $configuration) {
	$langues = array();
	$list_langues = sql_allfetsel("id_rubrique, titre", "spip_rubriques", "profondeur=0");

	foreach ($list_langues as $langue) {
		$langues[] = array(
			"title" => $langue['titre'],
			"url" 	=> url_site() . "/" . $langue['titre']
		);
	}

	return $langues;
}

//////////////////////////////////// THEMATIQUES ///////////////////////////////

/**
 * Détermine si la valeur du filtre "langue" pour la collection "thematiques" est valide.
 *
 * @param mixed  $valeur Valeur du filtre ffff.
 * @param &array $erreur Tableau initialisé avec les index identifiant l'erreur.
 *
 * @return bool `true` si la valeur est valide, `false` sinon.
 */
function thematiques_verifier_filtre_langue($valeur, &$erreur) {
	$est_valide = check_langue($valeur, $erreur);

	return $est_valide;
}

/**
 * Récupère les données de la collection "thematiques" éventuellement filtrés par les critères
 * additionnels positionnés dans la requête.
 *
 * @param array $conditions    Tableau des conditions SQL à appliquer au select et correspondant aux filtres
 *                             passés dans la requête.
 * @param array $filtres       Tableau des critères de filtrage additionnels à appliquer au select.
 * @param array $configuration Configuration de la collection.
 *
 * @return array Tableau des données de la collection.
 */
function thematiques_collectionner($conditions, $filtres, $configuration) {
	$mots = array();
	$langue = "";

	if ($filtres['langue']) {
		$langue = "?lang=" . $filtres['langue'];
	}

	$result = sql_allfetsel(
		"id_mot, titre, url, maj",
		"spip_mots, spip_urls",
		"id_mot=id_objet AND spip_urls.type='mot'"
	);

	if ($result) {
		foreach ($result as $mot) {
			$mots[] = array(
				"id" => $mot['id_mot'],
				"type" => "thematiques",
				"title" => extraire_multi($mot['titre'],$filtres['langue']),
				"url" => url_site() . "/" . $mot["url"] . $langue,
				"logo" => get_logo($mot, "id_mot"),
				"update" => $mot['maj']
			);
		}
	}

	return $mots;
}

/**
 * Détermine si la valeur d'une ressource "thematique" est valide (id_mot).
 *
 * @param string $valeur La valeur de la ressource demandée. La ressource appartient à une collection.
 * @param &array $erreur Tableau initialisé avec les index identifiant l'erreur.
 *
 * @return bool `true` si la valeur est valide, `false` sinon.
 */
function thematiques_verifier_ressource_id($valeur, &$erreur) {
	$est_valide = ($id = sql_allfetsel('id_mot', 'spip_mots', 'id_mot=' . intval($valeur)));

	if (!$est_valide) {
		$erreur['type'] = 'thematique_inexistante';
	}

	return $est_valide;
}

/**
 * Retourne la description complète d'une ressource thematique.
 *
 * @param string $ressource     La valeur de la ressource.
 * @param array  $filtres       Tableau des critères de filtrage additionnels à appliquer au select.
 * @param array  $configuration Configuration de la collection.
 *
 * @return array Le tableau descriptif de la ressource.
 */
function thematiques_ressourcer($ressource, $filtres, $configuration) {
	$lang = $filtres['langue'] ? $filtres['langue'] : "fr";
	$query = $filtres['langue'] ? "?lang=" . $filtres['langue'] : "";

	$res_mot = sql_allfetsel(
		"id_mot, titre, maj, url",
		"spip_mots, spip_urls",
		"id_mot=id_objet AND spip_urls.type='mot' AND id_mot=" . intval($ressource)
	);
	$mot = $res_mot[0];

	$thematique = array(
		"id" => $mot["id_mot"],
		"type" => "thematiques",
		"title" => extraire_multi($mot['titre'], $lang),
		"url" => url_site() . "/" . $mot["url"] . $query,
		"logo" => get_logo($mot, "id_mot"),
		"update" => $mot['maj']
	);

	$select = 'id_article, titre, url_site, maj, id_rubrique AS id_parent, id_trad';
	$from = 'spip_mots_liens, spip_articles';
	$where = array(
		"objet = 'article'",
		"id_objet = id_article",
		"statut = 'publie'",
		"id_mot = " . intval($ressource),
		"lang = " . sql_quote($lang)
	);

	$res_articles = sql_allfetsel($select, $from, $where);
	$articles = array();

	if ($res_articles) {
		$thematique["links"] = array();
		foreach ($res_articles as $article) {
			$thematique["links"][] = array(
				"id" => $article['id_article'],
				"type" => "liens",
				"title" => supprimer_numero($article['titre']),
				"url" => $article['url_site'],
				"logo" => get_logo($article, "id_article"),
				"update" => $article['maj'],
				"translations" => get_traductions($article)
			);
		}
	}

	return $thematique;
}

//////////////////////////////////// RUBRIQUES ///////////////////////////////

/**
 * Détermine si la valeur du filtre "langue" pour la collection "rubriques" est valide.
 *
 * @param mixed  $valeur Valeur du filtre ffff.
 * @param &array $erreur Tableau initialisé avec les index identifiant l'erreur.
 *
 * @return bool `true` si la valeur est valide, `false` sinon.
 */
function rubriques_verifier_filtre_langue($valeur, &$erreur) {
	$est_valide = check_langue($valeur, $erreur);

	return $est_valide;
}


/**
 * Récupère les données de la collection "rubriques" éventuellement filtrés par les critères
 * additionnels positionnés dans la requête.
 *
 * @param array $conditions    Tableau des conditions SQL à appliquer au select et correspondant aux filtres
 *                             passés dans la requête.
 * @param array $filtres       Tableau des critères de filtrage additionnels à appliquer au select.
 * @param array $configuration Configuration de la collection.
 *
 * @return array Tableau des données de la collection.
 */
function rubriques_collectionner($conditions, $filtres, $configuration) {
	$rubriques = array();
	$rubriques_hierarchie = array();
	$select = "id_rubrique, titre, descriptif, maj, spip_rubriques.id_parent, url, id_trad";
	$from = "spip_rubriques, spip_urls";
	$where = array(
		"id_rubrique=id_objet",
		"type='rubrique'",
		"statut='publie'",
		"profondeur=1"
	);
	$orderby = array("titre");

	if (!$conditions) {
		$conditions = array("lang='fr'");
	}

	$where = array_merge($where, $conditions);
	$result = sql_allfetsel($select, $from, $where, "", $orderby);

	foreach ($result as $rubrique) {
		$rub = array(
			'id' => $rubrique['id_rubrique'],
			'type' => 'rubrique',
			'title' => supprimer_numero($rubrique['titre']),
			"url" => url_site() . "/" . urls_arbo_generer_url_objet_dist($rubrique['id_rubrique'], "rubrique"),
			'descriptif' => $rubrique['descriptif'],
			"logo" => get_logo($rubrique, "id_rubrique"),
			'update' => $rubrique['maj'],
			"translations" => get_traductions($rubrique)
		);

		if ($enfants = liste_enfants($rubrique['id_rubrique'])) {
			$rub['children'] = $enfants;
		}

		$rubriques_hierarchie[] = $rub;
  }

	return $rubriques_hierarchie;
}

/**
 * Détermine si la valeur d'une ressource "rubrique" est valide (id_rubrique).
 *
 * @param string $valeur La valeur de la ressource demandée. La ressource appartient à une collection.
 * @param &array $erreur Tableau initialisé avec les index identifiant l'erreur.
 *
 * @return bool `true` si la valeur est valide, `false` sinon.
 */
function rubriques_verifier_ressource_id($valeur, &$erreur) {
	$est_valide = ($id = sql_allfetsel('id_rubrique', 'spip_rubriques', 'id_rubrique=' . intval($valeur)));

	if (!$est_valide) {
		$erreur['type'] = 'rubrique_inexistante';
	}

	return $est_valide;
}

/**
 * Retourne la description complète d'une ressource rubrique.
 *
 * @param string $ressource     La valeur de la ressource.
 * @param array  $filtres       Tableau des critères de filtrage additionnels à appliquer au select.
 * @param array  $configuration Configuration de la collection.
 *
 * @return array Le tableau descriptif de la ressource.
 */
function rubriques_ressourcer($ressource, $filtres, $configuration) {
	$rubrique = get_rubrique($ressource, $filtres);

	if ($rubrique['profondeur'] > 1) {
		$rubrique["parent"] = get_rubrique($rubrique["id_parent"]);
		unset($rubrique["parent"]["id_parent"]);
		unset($rubrique["parent"]["profondeur"]);
	}

	unset($rubrique["id_parent"]);
	unset($rubrique["profondeur"]);

	$select = 'id_article, titre, url_site, maj, id_rubrique AS id_parent, id_trad';
	$from = 'spip_articles';
	$where = array(
		"id_rubrique = " . intval($ressource),
		"statut = 'publie'"
	);

	if ($filtres['langue']) {
		$where[] = "lang=" . sql_quote($filtres['langue']);
	}

	$res_articles = sql_allfetsel($select, $from, $where);
	$articles = array();

	if ($res_articles) {
		$rubrique["links"] = array();
		foreach ($res_articles as $article) {
			$rubrique["links"][] = array(
				"id" => $article['id_article'],
				"type" => "liens",
				"title" => supprimer_numero($article['titre']),
				"url" => $article['url_site'],
				"logo" => get_logo($article, "id_article"),
				"update" => $article['maj'],
				"translations" => get_traductions($article),
			);
		}
	} else {
		$enfants = liste_enfants($ressource, false);
		if ($enfants) {
			$rubrique["children"] = $enfants;
		}
		
	}

	return $rubrique;
}

//////////////////////////////////// LIENS ///////////////////////////////

/**
 * Détermine si la valeur du filtre "langue" pour la collection "liens" est valide.
 *
 * @param mixed  $valeur Valeur du filtre langue.
 * @param &array $erreur Tableau initialisé avec les index identifiant l'erreur.
 *
 * @return bool `true` si la valeur est valide, `false` sinon.
 */
function liens_verifier_filtre_langue($valeur, &$erreur) {
	$est_valide = check_langue($valeur, $erreur);

	return $est_valide;
}

/**
 * Elabore la condition SQL correspondant au filtre search et à la valeur fournie.
 * Cette fonction est utile si la condition ne correspond pas à une égalité simple qui elle est construite
 * par le service REST Factory lui-même.
 *
 * @param mixed  $valeur Valeur du filtre search.
 *
 * @return string Condition calculée
 */
function liens_conditionner_search($recherche) {
	$mots = explode(" ", $recherche);
	$sql_clauses = array();
	$like = "";

	foreach ($mots as $mot) {
		if (strlen($mot) > 3) {
			$sql_clauses[] = "titre LIKE '%{$mot}%'";
		}
	}

	$sql = implode(' AND ', $sql_clauses);
	
	return $sql;
}


/**
 * Récupère les données de la collection "liens" éventuellement filtrés par les critères
 * additionnels positionnés dans la requête.
 *
 * @param array $conditions    Tableau des conditions SQL à appliquer au select et correspondant aux filtres
 *                             passés dans la requête.
 * @param array $filtres       Tableau des critères de filtrage additionnels à appliquer au select.
 * @param array $configuration Configuration de la collection.
 *
 * @return array Tableau des données de la collection.
 */
function liens_collectionner($conditions, $filtres, $configuration) {
	$liens = array();
	$select = "id_article, titre, descriptif, url_site, id_rubrique AS id_parent, id_trad, maj";
	$from = "spip_articles";
	$where = array(
		"statut='publie'"
	);
	$orderby = "maj DESC";

	if (!$filtres["langue"]) {
		$where[] = "lang='fr'";
	}


	$where = array_merge($where, $conditions);
	$result = sql_allfetsel($select, $from, $where, "", $orderby);

	foreach ($result as $article) {
		$parent = get_rubrique($article['id_parent']);
		unset($parent["id_parent"]);
		unset($parent["profondeur"]);

		$liens[] = array(
			'id' => $article['id_article'],
			'type' => 'liens',
			'title' => supprimer_numero($article['titre']),
			'url' => $article["url_site"],
			'descriptif' => $article['descriptif'],
			'logo' => get_logo($article, 'id_article'),
			'parent' => $parent,
			'update' => $article['maj'],
			"translations" => get_traductions($article)
		);
  }

	return $liens;
}

/**
 * Détermine si la valeur d'une ressource "lien" est valide (id_article).
 *
 * @param string $valeur La valeur de la ressource demandée. La ressource appartient à une collection.
 * @param &array $erreur Tableau initialisé avec les index identifiant l'erreur.
 *
 * @return bool `true` si la valeur est valide, `false` sinon.
 */
function liens_verifier_ressource_id($valeur, &$erreur) {
	$est_valide = ($id = sql_allfetsel('id_article', 'spip_articles', 'id_article=' . intval($valeur)));

	if (!$est_valide) {
		$erreur['type'] = 'lien_inexistant';
	}

	return $est_valide;
}

/**
 * Retourne la description complète d'une ressource lien.
 *
 * @param string $ressource     La valeur de la ressource.
 * @param array  $filtres       Tableau des critères de filtrage additionnels à appliquer au select.
 * @param array  $configuration Configuration de la collection.
 *
 * @return array Le tableau descriptif de la ressource.
 */
function liens_ressourcer($ressource, $filtres, $configuration) {
	$lien = array();
	$select = 'id_article, titre, descriptif, url_site, id_rubrique AS id_parent, id_trad, maj';
	$from = 'spip_articles';
	$where = array(
		"id_article = " . intval($ressource),
		"statut = 'publie'"
	);

	if ($filtres['langue']) {
		$where[] = "lang=" . sql_quote($filtres['langue']);
	}

	$res_article = sql_allfetsel($select, $from, $where);

	if ($res_article) {
		$article = $res_article[0];
		$parent = get_rubrique($article['id_parent']);
		unset($parent["id_parent"]);
		unset($parent["profondeur"]);

		$lien = array(
			'id' => $article['id_article'],
			'type' => 'liens',
			'title' => supprimer_numero($article['titre']),
			'url' => $article["url_site"],
			'descriptif' => $article['descriptif'],
			"logo" => get_logo($article, "id_article"),
			'parent' => $parent,
			'update' => $article['maj'],
			"translations" => get_traductions($article)
		);
	}

	return $lien;
}
