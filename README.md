# API REST pour Infojune

Il s'agit ici d'un plugin pour le CMS SPIP spécialement conçu pour [infojune.fr](https://infojune.fr) - mais qui peut être adapté - pour exposer les données du site sous forme d'une [API REST](https://fr.wikipedia.org/wiki/Representational_state_transfer).

Ce plugin est basé sur le plugin [ezrest](https://contrib.spip.net/REST-Factory-simplifier-les-API-REST).

## Consommation de l'API

L'API est exposée sur https://infojune.fr/api.

Vous pouvez récupérer :

- les langues utilisées dans les rubriques : <code>/api/langues</code>
- les rubriques avec un paramètre langue optionnel (fr par défaut) : <code>/api/rubriques?langue=en</code>
- les mots-clés avec un paramètre langue optionnel (fr par défaut) : <code>/api/thematiques?langue=en</code>

Pour afficher les informations d\'une rubrique ou d\'un mot-clé, il suffit de passer son ID.
Pour chaque rubrique ou mot-clé affiché, l\'API retourne les informations de la ressource demandée.
Exemples :

- <code>/api/rubriques/70</code>
- <code>/api/rubriques/28</code>
- <code>/api/thematiques/11</code>'

## Paramétrage serveur

### Apache

    RewriteRule ^api(\/(.*))?$ spip.php?action=api_http&arg=ezrest/$2 [QSA,L]

### NGINX

    rewrite ^/api(\/(.*))?$ /spip.php?action=api_http&arg=ezrest/$2 last;
