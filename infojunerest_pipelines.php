<?php
// Voir le fichier prefixe_pipelines.php.template du plugin ezrest

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function infojunerest_liste_ezcollection($collections) {
	if (!$collections) {
		$collections = array();
	}

	$cache = array(
		'type' => 'ezrest',
		//'duree' => 3600*24*30
		'duree' => 1
	);

	$collections['langues'] = array(
		'module' => 'infojunerest',
		'cache' => $cache,
	);
	
	$collections['rubriques'] = array(
		'module' => 'infojunerest',
		'cache' => $cache,
		'filtres'   => array(
			array(
				'critere'			=> 'langue',
				'vide_interdit'		=> true,
				'champ_nom'			=> 'lang',
				'format'			=> '/^(?=^[a-z]{2}$)[a-z]+$/'
			)
		),
		'ressource' => 'id'
	);

	$collections['thematiques'] = array(
		'module' => 'infojunerest',
		'cache' => $cache,
		'filtres'   => array(
			array(
				'critere'			=> 'langue',
				'vide_interdit'		=> true,
				'champ_nom'			=> 'lang',
				'format'			=> '/^(?=^[a-z]{2}$)[a-z]+$/'
			)
		),
		'ressource' => 'id'
	);

	$collections['liens'] = array(
		'module' => 'infojunerest',
		'cache' => $cache,
		'filtres'   => array(
			array(
				'critere'			=> 'langue',
				'vide_interdit'		=> true,
				'champ_nom'			=> 'lang',
				'format'			=> '/^(?=^[a-z]{2}$)[a-z]+$/'
			),
			array(
				'critere'	=> 'search',
				'format'	=> '/^\w+(\s+(\w+))*$/'
			)
		),
		'ressource' => 'id'
	);

	return $collections;
}
