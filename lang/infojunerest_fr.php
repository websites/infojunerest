<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'erreur_400_critere_lang_inexistant_titre'			=> 'Langue inexistante.',
	'erreur_400_critere_lang_inexistant_message'		=> 'Veuillez utiliser une langue valide.',
	'erreur_400_lien_inexistant_titre'							=> 'Lien inexistant.',
	'erreur_400_lien_inexistant_message'						=> 'Veuillez utiliser un lien valide.',
	'erreur_400_rubrique_inexistante_titre'					=> 'Rubrique inexistante.',
	'erreur_400_rubrique_inexistante_message'				=> 'Veuillez utiliser une rubrique valide.',
	'erreur_400_thematique_inexistante_titre'				=> 'Thématique inexistante.',
	'erreur_400_thematique_inexistante_message'			=> 'Veuillez utiliser une thématique valide.',
);
