<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'infojunerest_description' => 'Ce plugin est basé sur le plugin ezrest qui permet d\'exposer une API REST pour le site infojune.fr.
	Ce plugin expose :
		- les langues utilisées dans les rubriques : <code>/http.api/ezrest/langues</code>
		- les rubriques avec un paramètre langue optionnel (fr par défaut) : <code>/http.api/ezrest/rubriques?langue=en</code>
		- les mots-clés avec un paramètre langue optionnel (fr par défaut) : <code>/http.api/ezrest/thematiques?langue=en</code>
	Pour afficher les informations d\'une rubrique ou d\'un mot-clé, il suffit de passer son ID.
	Pour chaque rubrique ou mot-clé affiché, l\'API retourne la liste des sous-rubriques ou des articles concernés.
	
	Exemples :
		- <code>/http.api/ezrest/rubriques/70</code>
		- <code>/http.api/ezrest/rubriques/28</code>
		- <code>/http.api/ezrest/thematiques/11</code>',
	'infojunerest_slogan' => 'API REST pour Infojune.fr'
);
